from fastapi import FastAPI
# from pydantic import BaseModel

from models.database import engine
from models.inventory import inventory_model

from routers.inventory import inventory_router
from routers.users import users_router


app = FastAPI()
app.include_router(inventory_router.router)
app.include_router(users_router.router)

@app.get("/")
def hello():
    ''' for root API '''
    return {"Hello": "FastAPI"}

inventory_model.Base.metadata.create_all(engine)

# class InventoryBase(BaseModel):
#     description: str
#     price: float
#     stock: int

# fake_inventory = [
#     {'description': 'pencil', 'price': 15, 'stock': 15},
#     {'description': 'laptop', 'price': 5, 'stock': 20},
#     {'description': 'books', 'price': 25, 'stock': 30},
# ]

# @app.get("/")
# def get_all_inventory(version: int=1):
#     if version > 1:
#         return {'msg': 'data not avilable'}
#     else:
#         return fake_inventory


# @app.get("/{id}")
# def inventory_by_id(id: int):
#     item = fake_inventory[id - 1] # list index start at 0
#     return item

# @app.post("/")
# def post_api(inventory: InventoryBase):
#     print(inventory)
#     fake_inventory.append(inventory)
#     return inventory


# @app.put("/{id}")
# def put_api(id: int, inventory: InventoryBase):
#     fake_inventory[id-1].update(**inventory.dict())
#     item = fake_inventory[id-1]
#     return item


# @app.delete("/{id}")
# def delete_api(id: int):
#     item = fake_inventory.pop(id-1)
#     return item
