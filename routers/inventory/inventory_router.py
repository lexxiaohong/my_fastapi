from fastapi import APIRouter
from pydantic import BaseModel

from sqlalchemy.orm import Session
from models.database import get_db
from fastapi import Depends
from models.inventory.inventory_model import InventoryBase
from routers.inventory import inventory_controller

# add prefix for path ไม่ให้ชนกัน
router = APIRouter(
    prefix="/inventory",
    tags=["inventory"]
)

# class InventoryBase(BaseModel):  # not use for exmaple that not use db (use line 7 instead)
#     description: str
#     price: float
#     stock: int

fake_inventory = [
    {'description': 'pencil', 'price': 15, 'stock': 15},
    {'description': 'laptop', 'price': 5, 'stock': 20},
    {'description': 'books', 'price': 25, 'stock': 30},
]

@router.get("/")
def get_all_inventory(version: int=1):
    pass
    # if version > 1:
    #     return {'msg': 'data not avilable'}
    # else:
    #     return fake_inventory


@router.get("/{id}")
def inventory_by_id(id: int):
    # item = fake_inventory[id - 1] # list index start at 0
    # return item
    pass

@router.post("/")
def create_inventory(request: InventoryBase, db: Session = Depends(get_db)):
    return inventory_controller.create(db, request)


@router.put("/{id}")
def put_api(id: int, inventory: InventoryBase):
    fake_inventory[id-1].update(**inventory.dict())
    item = fake_inventory[id-1]
    return item


@router.delete("/{id}")
def delete_api(id: int):
    item = fake_inventory.pop(id-1)
    return item
