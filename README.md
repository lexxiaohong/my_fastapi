## create virtual environment
```
python3 -m venv env
source env/bin/activate
.\env\Scripts\activate (for Windows)
deactivate  (for deactivate that env or close that terminall)
```
## install fastapi
```
pip3 install fastapi
```
## install uvicorn  for run server
```
pip3 install "uvicorn[standard]"
```
## install formatter
black main.py
black .  (every file in project)
```
pip3 install black
```
## Run server
```
uvicorn main:app --reload
```
พิมพ์ /docs ต่อท้าย url จะได้ document api มาให้เลย (Swagger UI)
http://127.0.0.1:8000/docs

## install DB
```
pip3 install sqlalchemy
```